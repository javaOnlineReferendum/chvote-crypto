# CHVote: Crypto
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/chvote-crypto/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/chvote-crypto/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-crypto&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-crypto)

This library  holds all the cryptographic primitives needed for the CHVote project, except the elements defined at the
lowest level in the protocol specification.


# Usage

## Add to your project

Maven:
```xml
<!-- API Module -->
<dependency>
    <groupId>ch.ge.ve</groupId>
    <artifactId>crypto-api</artifactId>
    <version>1.2.0</version>
</dependency>

<!-- Implementation module -->
<dependency>
    <groupId>ch.ge.ve</groupId>
    <artifactId>crypto-impl</artifactId>
    <version>1.2.0</version>
</dependency>
```

# Building

## Pre-requisites

* JDK 8
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/shared-libraries/chvote-crypto/blob/master/LICENSE) 
license.
