/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.impl.authentication;

import ch.ge.ve.crypto.authentication.AuthenticationHasher;
import ch.ge.ve.crypto.authentication.HashedAuthentication;
import ch.ge.ve.crypto.exception.AuthenticationHasherConfigException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * authentication Hasher implementation
 * <ul>
 * <li>Algorithm: PBKDF2WithHmacSHA1</li>
 * <li>Salt size: 128 bits</li>
 * <li>Iteration count:  according to the input parameter</li>
 * </ul>
 */
public class AuthenticationHasherImpl implements AuthenticationHasher {


  private static final int KEY_LENGTH = 64 * 8;
  private static final int SALT_SIZE  = 16;

  @Override
  public byte[] getSalt() {
    SecureRandom secureRandom;
    try {
      secureRandom = SecureRandom.getInstance("SHA1PRNG");
      byte[] salt = new byte[SALT_SIZE];
      secureRandom.nextBytes(salt);
      return salt;
    } catch (NoSuchAlgorithmException e) {
      throw new AuthenticationHasherConfigException("can't get cryptographic salt", e);
    }
  }

  @Override
  public HashedAuthentication hash(String authentificationInformation, byte[] salt, int iterations) {

    HashedAuthentication hashedAuthentication;
    try {
      PBEKeySpec spec = new PBEKeySpec(authentificationInformation.toCharArray(), salt, iterations, KEY_LENGTH);

      SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      byte[] hash = skf.generateSecret(spec).getEncoded();

      hashedAuthentication = new HashedAuthentication(iterations, salt, hash);

    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new AuthenticationHasherConfigException("can't initialize authentication hasher", e);
    }
    return hashedAuthentication;
  }

}
