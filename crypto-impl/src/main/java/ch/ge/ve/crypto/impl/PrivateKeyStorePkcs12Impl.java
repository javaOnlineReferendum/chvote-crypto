/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.impl;

import ch.ge.ve.crypto.PrivateKeyStore;
import ch.ge.ve.crypto.exception.CryptoException;
import ch.ge.ve.crypto.exception.CryptoRuntimeException;
import ch.ge.ve.crypto.exception.KeyStoreNotEmptyException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Objects;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Private key store implementation using the PKCS#12 standard, using strong password protection attributes:
 * <ul>
 * <li>Algorithm: PBEWithHmacSHA512AndAES_256</li>
 * <li>Salt size: 160 bits</li>
 * <li>Iteration count: random between 200.000 and 275.000</li>
 * </ul>
 */
public final class PrivateKeyStorePkcs12Impl implements PrivateKeyStore {
  private static final String KEY_STORE_TYPE = "PKCS12";
  private static final String ALIAS          = "key";

  private static final String PROTECTION_ALGORITHM  = "PBEWithHmacSHA512AndAES_256";
  private static final int    SALT_BYTES            = 20;
  private static final int    ITERATION_COUNT_BASE  = 200_000;
  private static final int    ITERATION_COUNT_RANGE = 75_000;

  private final Path         pkcs12File;
  private final SecureRandom secureRandom;

  /**
   * @param pkcs12File path to the pkcs#12 keystore file to write to or to read from.
   *
   * @throws NullPointerException   if the {@code pkcs12File} is {@code null}
   * @throws CryptoRuntimeException if the keystore cannot be initialized properly
   */
  public PrivateKeyStorePkcs12Impl(Path pkcs12File) {
    this.pkcs12File = Objects.requireNonNull(pkcs12File);
    try {
      this.secureRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
    } catch (GeneralSecurityException e) {
      throw new CryptoRuntimeException("Cannot initialize the keystore", e);
    }
  }

  @Override
  public void storeKey(byte[] privateKey, char[] passphrase) throws KeyStoreNotEmptyException {
    Objects.requireNonNull(privateKey);
    Objects.requireNonNull(passphrase);
    if (!isEmpty()) {
      throw new KeyStoreNotEmptyException("The keystore file must be empty");
    }
    try {
      KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
      keyStore.load(null, null); // Initialize a blank keystore
      // Note that we're not necessarily storing an AES secret key here...
      // but this is the simplest way we found to store "raw data" in a key store...
      SecretKey key = new SecretKeySpec(privateKey, "AES");
      KeyStore.PasswordProtection passwordProtection = generatePasswordProtection(passphrase);
      keyStore.setEntry(ALIAS, new KeyStore.SecretKeyEntry(key), passwordProtection);
      try (OutputStream outputStream = Files.newOutputStream(pkcs12File)) {
        keyStore.store(outputStream, passphrase);
      }
    } catch (GeneralSecurityException | IOException e) {
      throw new CryptoRuntimeException("Cannot store key", e);
    }
  }

  private boolean isEmpty() {
    try {
      return Files.notExists(pkcs12File) || Files.size(pkcs12File) == 0L;
    } catch (IOException e) {
      throw new CryptoRuntimeException("Can't read " + pkcs12File, e);
    }
  }

  private KeyStore.PasswordProtection generatePasswordProtection(char[] passphrase) {
    byte[] salt = generateSalt();
    int iterationCount = generateIterationCount();
    return new KeyStore.PasswordProtection(passphrase, PROTECTION_ALGORITHM,
                                           new PBEParameterSpec(salt, iterationCount));
  }

  private byte[] generateSalt() {
    byte[] salt = new byte[SALT_BYTES];
    secureRandom.nextBytes(salt);
    return salt;
  }

  private int generateIterationCount() {
    int iterationCount = ITERATION_COUNT_BASE;
    iterationCount += secureRandom.nextDouble() * ITERATION_COUNT_RANGE;
    return iterationCount;
  }

  @Override
  public byte[] loadKey(char[] passphrase) throws CryptoException {
    Objects.requireNonNull(passphrase);
    try (InputStream inputStream = Files.newInputStream(pkcs12File)) {
      KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
      keyStore.load(inputStream, passphrase);
      Key key = keyStore.getKey(ALIAS, passphrase);
      if (key == null) {
        throw new CryptoException("Invalid key store");
      }
      return key.getEncoded();
    } catch (GeneralSecurityException | IOException e) {
      throw new CryptoException("Cannot get private key from the keystore", e);
    }
  }
}
