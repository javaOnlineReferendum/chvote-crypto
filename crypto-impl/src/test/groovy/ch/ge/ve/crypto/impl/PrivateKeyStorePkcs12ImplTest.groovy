/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.impl

import ch.ge.ve.crypto.exception.CryptoException
import ch.ge.ve.crypto.exception.KeyStoreNotEmptyException
import java.nio.file.Files
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec
import spock.lang.Specification

class PrivateKeyStorePkcs12ImplTest extends Specification {
  static def prng = new SecureRandom()
  static def passphrase = "key used for testing".toCharArray()
  static def wrongPassphrase = "not the key used for testing".toCharArray()

  def "storing a key should generate some content"() {
    given:
    def privateKey = randomBytes()
    def tempFile = Files.createTempFile("keystore", "p12")

    when:
    def keystore = new PrivateKeyStorePkcs12Impl(tempFile)
    keystore.storeKey(privateKey, passphrase)

    then:
    Files.size(tempFile) > 0L
  }

  def "storing a key twice should raise an exception"() {
    given:
    def privateKey = randomBytes()
    def tempFile = Files.createTempFile("keystore", "p12")

    when:
    def keystore = new PrivateKeyStorePkcs12Impl(tempFile)
    keystore.storeKey(privateKey, passphrase)
    keystore.storeKey(privateKey, passphrase)

    then:
    thrown(KeyStoreNotEmptyException)
  }

  def "storing a key into a non empty file should raise an exception"() {
    given:
    def tempFile = Files.createTempFile("keystore", "p12")
    tempFile.write("Not empty!")

    when:
    def keystore = new PrivateKeyStorePkcs12Impl(tempFile)
    keystore.storeKey(randomBytes(), passphrase)

    then:
    thrown(KeyStoreNotEmptyException)
  }

  def "loadKey should retrieve the key when given the right passphrase"() {
    given:
    def privateKey = randomBytes()
    def tempFile = privateKeyStoreFile(privateKey)

    when:
    def loadedKeystore = new PrivateKeyStorePkcs12Impl(tempFile)
    def loadedKey = loadedKeystore.loadKey(passphrase)

    then:
    loadedKey == privateKey
  }

  def "loadKey should not retrieve the key when given the wrong passphrase"() {
    given:
    def tempFile = privateKeyStoreFile(randomBytes())

    when:
    def loadedKeystore = new PrivateKeyStorePkcs12Impl(tempFile)
    loadedKeystore.loadKey(wrongPassphrase)

    then:
    thrown(CryptoException)
  }

  def "loadKey should fail if it does not find a key with the expected alias"() {
    given:
    def tempFile = Files.createTempFile("keystore", "p12")
    def keyStore = KeyStore.getInstance("PKCS12")
    keyStore.load(null, null)
    SecretKey key = new SecretKeySpec(randomBytes(), "AES")
    KeyStore.PasswordProtection passwordProtection = new KeyStore.PasswordProtection(passphrase)
    keyStore.setEntry("not the expected alias!", new KeyStore.SecretKeyEntry(key), passwordProtection)
    OutputStream outputStream = Files.newOutputStream(tempFile)
    keyStore.store(outputStream, passphrase)
    outputStream.close()

    when:
    def loadedKeystore = new PrivateKeyStorePkcs12Impl(tempFile)
    loadedKeystore.loadKey(passphrase)

    then:
    thrown(CryptoException)
  }

  def randomBytes(int length = 224) {
    def privateKey = new byte[length]
    prng.nextBytes(privateKey)
    return privateKey
  }

  def privateKeyStoreFile(byte[] privateKey) {
    def tempFile = Files.createTempFile("keystore", "p12")
    def output = Files.newOutputStream(tempFile)
    def keystore = new PrivateKeyStorePkcs12Impl(tempFile)
    keystore.storeKey(privateKey, passphrase)
    output.close()
    return tempFile
  }
}
