/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto;

import ch.ge.ve.crypto.exception.CryptoException;
import ch.ge.ve.crypto.exception.CryptoRuntimeException;
import ch.ge.ve.crypto.exception.KeyStoreNotEmptyException;

/**
 * Keystore designed to store securely a unique private key, protected using a passphrase.
 */
public interface PrivateKeyStore {
  /**
   * Stores the key into a private key store protected by a passphrase.
   *
   * @param privateKey private key to protect
   * @param passphrase user chosen passphrase used to protect the key
   *
   * @throws NullPointerException      if one of the given arguments is {@code null}
   * @throws KeyStoreNotEmptyException if the key store is not empty (key store is immutable)
   * @throws CryptoRuntimeException    if an unexpected error occurs during the process
   */
  void storeKey(byte[] privateKey, char[] passphrase) throws KeyStoreNotEmptyException;

  /**
   * Retrieves a key protected by a password.
   *
   * @param passphrase user chosen passphrase used to protect the key
   *
   * @return the key in raw data
   *
   * @throws NullPointerException if the {@code passphrase} is {@code null}
   * @throws CryptoException      if the key cannot be loaded from the key store
   */
  byte[] loadKey(char[] passphrase) throws CryptoException;
}
